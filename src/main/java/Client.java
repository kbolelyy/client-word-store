import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {


    /*java -jar client.jar 0.0.0.0 add hello алло привет здравствуйте*/
    public static void main(String[] args) {

        if (args.length >= 3) {

            String command = args[1];
            if (!(command.equals("add") || command.equals("get") || command.equals("delete"))){
                System.out.format("command %s not exist", command);
                System.exit(69);
            }

            ObjectInputStream ois = null;
            ObjectOutputStream ous = null;
            Socket socket = null;
            try {
                System.out.println("Connection to server address: " + args[0]);
                socket = new Socket(args[0], 8080);
                System.out.println("Connection to server successfully: " + socket.getRemoteSocketAddress());

                //получаем слова из аргументов которые должны быть добавлены в словарь
                String[] words = new String[args.length - 3];
                int k = 0;
                for (int i = 3; i < args.length; i++) {
                    words[k] = args[i];
                    k++;
                }

                ous = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());

                ous.writeObject(new MessageObject(command, args[2], words));
                ous.flush();


                System.out.println("Waiting response from server....");
                System.out.println(ois.readObject());

                socket.close();
            } catch (IOException | ClassNotFoundException e) {
                System.err.println(e);
                e.printStackTrace();
            } finally {
                try {
                    ous.close();
                    ois.close();
                    socket.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        } else {
            System.out.println("Not enough count of arguments. Check arguments please.");
        }

    }

}



