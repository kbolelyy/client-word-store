import java.io.Serializable;
import java.util.Arrays;

class MessageObject implements Serializable {


    private String command;
    private String keyWord;
    private String[] words;

    MessageObject(String command, String keyWord, String[] words) {

        this.command = command;
        this.keyWord = keyWord;
        this.words = words;
    }


    String getCommand() {
        return command;
    }

    String getKeyWord() {
        return keyWord;
    }

    String[] getWords() {
        return words;
    }

    @Override
    public String toString() {
        return "MessageObject{" +
                "command='" + command + '\'' +
                ", keyWord='" + keyWord + '\'' +
                ", words=" + Arrays.toString(words) +
                '}';
    }
}

